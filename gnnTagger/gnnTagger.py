import torch
import torch.nn as torch_nn
import torch.nn.functional as F
import os
import sys
import types
import uproot
import random 
import math 
import numpy as np
import awkward as ak
import matplotlib.pyplot as plt
from torch.nn import Linear
from torch_scatter import segment_coo, scatter
from torch_geometric.nn import global_mean_pool
from functools import partial
from tqdm import tqdm
from sklearn.metrics import roc_curve
from sklearn.model_selection import train_test_split

# Import custom functions
from preprocessing import toNumpy
from preprocessing import toTorchTensor
from preprocessing import buildSCalesOffsets
from preprocessing import toOffset
from preprocessing import batch

#############################################################################################################
# NN parameters 
#############################################################################################################

nEpochs      = 200
batchSize    = 1000
learningRate = 0.001

#############################################################################################################
# Reading inputs 
#############################################################################################################

# reading the input
filename  = "./inputs_singlecBackground.root"

# Get the TTree inside it with uproot.
tree = uproot.open(filename+":analysis")

# Features, event level and graph variables, labels
features = [#'_Tracks_pt', 
            #"_Tracks_eta",          
            #"_Tracks_e",           
            "_Tracks_d0",
            "_Tracks_z0SinTheta",
            "_Tracks_deta",
            "_Tracks_dphi",
            "_Tracks_qOverP",
            "_Tracks_IP3D_signed_d0_significance",
            "_Tracks_IP3D_signed_z0_significance",
            "_Tracks_phiUncert",
            "_Tracks_thetaUncert",
            "_Tracks_qOverPUncert",
            "_NumPixHits",
            "_NumSCTHits",
            "_NumIPLHits",
            "_NumNIPLHits",
            "_NumIPLSharedHits",
            "_NumIPLSplitHits",
            "_NumPixSharedHits",
            "_NumPixSplitHits",
            "_NumSCTSharedHits",
            "_NumPixHoles",
            "_NumSCTHoles",
            "_Tracks_leptonID"
           ]

globalFeatures  = ['_CaloJet_pt', 
                   '_CaloJet_eta' 
                  ]

auxVars  = [#'_CaloJet_pt', 
            #'_CaloJet_eta', 
            '_nTracks',
            'NumberOfEdges',
            'NumberOfUnions',
           # 'MCEventWeight'
           ]

edges    = ['Tracks_recieverI', 'Tracks_senderI']
edgeAuxs = ['EdgeRecieverI', 'EdgeSenderI']
targets  = ['_TrueLabel']

# Load all the above branches
arrays = tree.arrays(features+edges+targets+auxVars+edgeAuxs+globalFeatures)

# Print nEvents and nFeats
nFeatures       = len(features)
nGlobalFeatures = len(globalFeatures)
nEvents         = len(arrays)
print("=================================================")
print(f"Input sample with: * {nEvents} events           ")
print(f"                   * {nFeatures} track features ")
print("=================================================")

# Get rid of 0-track/1-track jets (to be revisited)
mask1  = ak.any(arrays._nTracks==0, axis=1)
arrays = arrays[~mask1]

mask2  = ak.any(arrays._nTracks==1, axis=1)
arrays = arrays[~mask2]

# Print nEvents after cut
nEvents = len(arrays)
print(f"After 0-track cut: * {nEvents} events           ")
print("=================================================")

#############################################################################################################
# Train/test/validation split 
#############################################################################################################
arrays_train, arrays_test_ = train_test_split(arrays, test_size=0.5, random_state=42)
nEvents_train = len(arrays_train)

arrays_test, arrays_valid  = train_test_split(arrays_test_, test_size=0.33, random_state=42)
nEvents_test  = len(arrays_test )
nEvents_valid = len(arrays_valid)

# train_test_split returns lists, going back to awkwards
arrays_train = ak.Array(arrays_train) 
arrays_test  = ak.Array(arrays_test ) 
arrays_valid = ak.Array(arrays_valid) 

## Print nEvents for training/testing sample
nEvents_train = len(arrays_train)
nEvents_test  = len(arrays_test )
print(f"After splitting  : * {nEvents_train} train events")
print(f"                   * {nEvents_test } test  events")
print(f"                   * {nEvents_valid} valid events")
print("=================================================")

"""
# Input (edge) test 
# The total number of edges is:
ne1 = len(ak.ravel(arrays.Tracks_senderI) )
# It should be the same as:
ne2 = ak.sum(arrays.NumberOfEdges)
print("Edge test passed? ",ne1==ne2)
print("=================================================")
"""

#############################################################################################################
# Conversions and preprocessing 
#############################################################################################################

# Automatically convert all arrays and collect them into a simple python object:
tensors_train = types.SimpleNamespace() # a basic python object
tensors_test  = types.SimpleNamespace() # a basic python object
tensors_valid = types.SimpleNamespace() # a basic python object
tensors_all   = types.SimpleNamespace() # a basic python object

# This just sets tensors.X = toTorchTensor(X):
for f in arrays_train.fields: setattr(tensors_train, f, toTorchTensor(arrays_train[f]))
for f in arrays_test .fields: setattr(tensors_test,  f, toTorchTensor(arrays_test [f]))
for f in arrays_valid.fields: setattr(tensors_valid, f, toTorchTensor(arrays_valid[f]))
for f in arrays.fields:       setattr(tensors_all,   f, toTorchTensor(arrays[f]      ))

"""
for k in tensors.__dict__: 
    if tensors.__dict__[k] is not None: 
        print(f"{k}  {len(tensors.__dict__[k])}")
"""

## Build tensor view on num of nodes/tracks
tensors_train.jetPt    = toTorchTensor(arrays_train._CaloJet_pt   )
tensors_train.jetEta   = toTorchTensor(arrays_train._CaloJet_eta  )
tensors_train.numNodes = toTorchTensor(arrays_train._nTracks      )
tensors_train.numEdges = toTorchTensor(arrays_train.NumberOfEdges )
#tensors_train.evWeight = toTorchTensor(arrays_train.MCEventWeight )

tensors_test.jetPt    = toTorchTensor(arrays_test._CaloJet_pt   )
tensors_test.jetEta   = toTorchTensor(arrays_test._CaloJet_eta  )
tensors_test.numNodes = toTorchTensor(arrays_test._nTracks      )
tensors_test.numEdges = toTorchTensor(arrays_test.NumberOfEdges )
#tensors_test.evWeight  = toTorchTensor(arrays_test.MCEventWeight )

tensors_valid.jetPt    = toTorchTensor(arrays_valid._CaloJet_pt   )
tensors_valid.jetEta   = toTorchTensor(arrays_valid._CaloJet_eta  )
tensors_valid.numNodes = toTorchTensor(arrays_valid._nTracks      )
tensors_valid.numEdges = toTorchTensor(arrays_valid.NumberOfEdges )
#tensors_valid.evWeight = toTorchTensor(arrays_valid.MCEventWeight )

tensors_all.jetPt    = toTorchTensor(arrays._CaloJet_pt   )
tensors_all.jetEta   = toTorchTensor(arrays._CaloJet_eta  )
tensors_all.numNodes = toTorchTensor(arrays._nTracks      )
tensors_all.numEdges = toTorchTensor(arrays.NumberOfEdges )
#tensors_all.evWeight   = toTorchTensor(arrays.MCEventWeight )

# Feature normalization (Apply same normalization to train and test dataset) 
scales_train, offsets_train = buildSCalesOffsets(features, tensors_all)
scales_test,  offsets_test  = buildSCalesOffsets(features, tensors_all)
scales_valid, offsets_valid = buildSCalesOffsets(features, tensors_all)

# Indices manipulation : batch preparation
# Node offsets from the number of tracks and edges per jet
tensors_train.nodeOffsets = torch.tensor(toOffset(arrays_train._nTracks))
tensors_train.edgeOffsets = torch.tensor(toOffset(arrays_train.NumberOfEdges ))
tensors_train.mpOffsets   = torch.tensor(toOffset(arrays_train.NumberOfUnions ))

tensors_test.nodeOffsets  = torch.tensor(toOffset(arrays_test._nTracks ))
tensors_test.edgeOffsets  = torch.tensor(toOffset(arrays_test.NumberOfEdges  ))
tensors_test.mpOffsets    = torch.tensor(toOffset(arrays_test.NumberOfUnions ))

tensors_valid.nodeOffsets = torch.tensor(toOffset(arrays_valid._nTracks))
tensors_valid.edgeOffsets = torch.tensor(toOffset(arrays_valid.NumberOfEdges ))
tensors_valid.mpOffsets   = torch.tensor(toOffset(arrays_valid.NumberOfUnions ))

#############################################################################################################
# Torch modules and model 
#############################################################################################################

# Module dedicated to normalization of inputs :
class Normalization(torch.nn.Module):
    """Applies a linear normalization to the inputs : y=scale*x+offset where
    scale and offset are user given, fixed parameters.
    """
    def __init__(self, scales, offsets, **args):
        super().__init__(**args)
        # Declare some NON trainable parameters (pass the requires_grad=False option)
        self.scales  = torch.nn.Parameter(scales,  requires_grad=False)
        self.offsets = torch.nn.Parameter(offsets, requires_grad=False)

    def forward(self, x):
        u= self.scales * x + self.offsets
        u=u.to(torch.float32)
        return u

# Module dedicated to translate indices :
class ShiftNodeIndices(torch.nn.Module):
    """ The edges are given as indices in each graph. But the data is organised by batches
          * How to shift the value of the node indices in the graph so they correspond to the indices in the batch?
    
        Write a Module dedicated to translate indices :
    """
    def forward(self, edgeI, numNodesPerGraph, numEdgesPergraph):
        """ inputs refer to the current batch :
            edgI is a 2D array of shape (nEdgesIntheBatch, 2) as build in the batch function above, numEdgesPergraph and numNodesPerGraph are 1D of shape (nGraphsIntheBatch,)
        """
        # We need to recalculate the node offsets in the batch. Intialize them at 0 :
        offset = torch.zeros( len(numNodesPerGraph), dtype=int )

        # Let's use the cumsum() function (cumsum = cummulative sum )
        cs = torch.cumsum( numNodesPerGraph , dim=0)

        # Careful! cs = [n0 , n0+n1, n0+n1+n2, ...] but what we want is [0, n0, n0+n1, ...]
        offset[1:] += cs[:-1] # now we have offset == [0, n0, n0+n1, ...]

        # Finally we need to add these offsets to each of the corresponding edges, so we duplicate the offsets for each of the edges:
        offset_atedges = torch.repeat_interleave(offset, numEdgesPergraph)

        edgeI = edgeI+offset_atedges.view(-1,1)

        return edgeI

# Basic MLP module
class MLP(torch.nn.Module):
    """We'll use 3 MLP in the full GNN. So we write a Module to represent mlp using several nn.Linear and a nn.Sequential, following something like :
                          mlp = Sequential( Linear(n0,n1), activation(), 
                                            Linear(n1,n2), activation(), 
                                            Linear(n2,n3), activation(), 
                                            ...)"""
    def __init__(self, numInput, layers=[], activation=torch.nn.Sigmoid(), last_activation=None):
        super().__init__()
        nin = numInput
        seq = []
        for nout in layers:
            seq += [ torch.nn.Linear(nin, nout), activation ]
            nin = nout
        if last_activation is None: last_activation=activation
        seq[-1] = last_activation

        self.model = torch.nn.Sequential(*seq)

    def forward(self, x):
        return self.model(x)

# Module that performs the average of *node* features over connected nodes
class AverageConnNodes(torch.nn.Module):
    """Performs aggregation of nodes connected in a graph.
       The scatter function takes, for every node, the average of the connected nodes. This is done separately for every variable.
       For example, if we have three fully connected nodes with 1 feature each [f0, f1, f2], this functions gives you [avg(f1,f2), avg(f0,f2), avg(f0,f1)]
       If we have two variables instead [[f0, j0], [f1,j1], [f2,j2]], we get [ [avg(f1,f2), avg(j1,j2)], ...  ]
    """
    def forward(self, edgI, nodeFeatures):
        senderI   = edgI[:,0]
        recieverI = edgI[:,1]

        # Take node features at each possible sending node: 
        featuresAtSendNode = nodeFeatures[senderI] 
        nNodes             = nodeFeatures.shape[0]

        # Use the torch_scatter.scatter() function to sum the features at all possible sending nodes onto the position of recieving nodes
        # See https://pytorch-scatter.readthedocs.io/en/latest/functions/scatter.html
        avgFeatures = scatter(featuresAtSendNode, recieverI, dim_size=nNodes, reduce='mean', dim=0)
        return avgFeatures

# Module that performs the average of *edge* features over connected nodes
class AverageConnEdges(torch.nn.Module):
    """
    """
    def forward(self, edgI, nodeFeatures, edgeFeatures, edgeSendI, edgeReciI):
        senderI   = edgI[:,0]
        recieverI = edgI[:,1]

        edgeSI = edgeSendI.type(torch.int64) 
        edgeRI = edgeReciI.type(torch.int64)

        # Take node features at each possible sending node:
        featuresAtSendEdge = edgeFeatures[edgeSI ]
        nNodes             = nodeFeatures.shape[0]

        # Use the torch_scatter.scatter() function to sum the features at all possible sending edges onto the position of recieving nodes
        # See https://pytorch-scatter.readthedocs.io/en/latest/functions/scatter.html (picture illustrates perfectly)
        avgFeatures = scatter(featuresAtSendEdge, edgeRI, dim_size=nNodes, reduce='mean', dim=0)
        
        return avgFeatures

# Module that performs the average of *node* features over the connected edges
class AverageNodesInEdge(torch.nn.Module):
    def forward(self, edgI, nodeFeatures):
        senderI   = edgI[:,0]
        recieverI = edgI[:,1]

        # Take node features at each possible sending/reciever node: 
        featuresAtSendNode    = nodeFeatures[senderI]
        featuresAtReciverNode = nodeFeatures[recieverI]
        
        avgFeatures = (featuresAtSendNode + featuresAtReciverNode)/2
        return avgFeatures

# Module that performs the average of 
class AverageEdgesInNodes(torch.nn.Module):
    def forward(self, edgI, nodeFeatures, edgeFeatures, edgeSendI, edgeReciI):
        senderI   = edgI[:,0]
        recieverI = edgI[:,1]

        indexes = torch.cat((recieverI, senderI))
        inputs  = torch.cat((edgeFeatures, edgeFeatures))
        nNodes  = nodeFeatures.shape[0]

        avgFeatures = scatter(inputs, indexes, dim_size=nNodes, reduce='mean', dim=0)
        
        return avgFeatures

# Message passing module combining a MLP and the `AverageConnNodes`
class MessagePassing(torch.nn.Module):
    def __init__(self, numInput, layers=[], activation=torch.nn.Mish(), last_activation=None):
    #def __init__(self, numInput, layers=[], activation=torch.nn.Sigmoid(), last_activation=None):
        super().__init__()
        self.avgNodes = AverageConnNodes()
        self.mlp      = MLP(numInput,layers,activation,last_activation)

    def forward(self, nodeFeatures, edgI):
        avg = self.avgNodes(edgI, nodeFeatures)
        out = self.mlp(avg)

        return out

# Message passing module to pass edge information to nodes 
class EdgesToNodes(torch.nn.Module):
    #def __init__(self, numInput, layers=[], activation=torch.nn.Sigmoid(), last_activation=None):
    def __init__(self, numInput, layers=[], activation=torch.nn.Mish(), last_activation=None):
        super().__init__()
        self.avgEdges = AverageEdgesInNodes()
        self.mlp      = MLP(numInput, layers, activation, last_activation)

    def forward(self, nodeFeatures, edgeFeatures, edgI, edgeSendI, edgeReciI):
        avg = self.avgEdges(edgI, nodeFeatures, edgeFeatures, edgeSendI, edgeReciI)
        out = self.mlp(avg)

        return out

# Message passing module to pass node information to edges
class NodesToEdges(torch.nn.Module):
    #def __init__(self, numInput, layers=[], activation=torch.nn.Sigmoid(), last_activation=None):
    def __init__(self, numInput, layers=[], activation=torch.nn.Mish(), last_activation=None):
        super().__init__()
        self.avgNodes = AverageNodesInEdge()
        self.mlp      = MLP(numInput, layers, activation, last_activation)

    def forward(self, nodeFeatures, edgI):
        avg = self.avgNodes(edgI, nodeFeatures)
        out = self.mlp(avg)

        return out

# Activation for final classification 
class lastActivation(torch.nn.Module):
    """Define an activation giving values between [0,1]"""
    def forward(self,x):
        return torch.sigmoid(x)

# Loss Function 
#def jetBCE(predictions, numNodesPerGraph, trueLabel, batchSize, weights):
def jetBCE(predictions, numNodesPerGraph, trueLabel, batchSize):
    """Binary cross entropy loss function"""
    trueLabel   = trueLabel.view(batchSize,1)
    trueLabel   = trueLabel.float()
    #weights     = weights.view(batchSize,1)
    #loss        = torch_nn.BCELoss(weights)
    loss        = torch_nn.BCELoss()
    return loss(predictions, trueLabel)
    
# Complete GNN 
class GNN(torch.nn.Module):
    def __init__(self, inputTensors):
        super().__init__()
        self.normGlobals = Normalization(*buildSCalesOffsets(globalFeatures, inputTensors))
        self.normNodes   = Normalization(*buildSCalesOffsets(features, inputTensors))
        self.shiftNI     = ShiftNodeIndices()

        # Initial NN: applies one NN *to each track* independently
       # self.preMLP   = MLP(nFeatures, [256,128,64,32] )
        self.preMLP   = MLP(nFeatures, [128,64,32] )

        # Message passing layers: node to edges, edges to nodes
        self.mpNodesToEdges = NodesToEdges(32, [128,64, 32] )
        self.mpEdgesToNodes = EdgesToNodes(32, [128,64, 32] )

        # Message passing layers: node to nodes
        self.mpBlock1 = MessagePassing(32, [64,32] )
        self.mpBlock2 = MessagePassing(32, [64,32] )
        self.mpBlock3 = MessagePassing(32, [64,32] )
       
        # Mid classification
        self.midMLP   = MLP( 32, [64,32], last_activation=lastActivation() )

        # Last node classification
        self.postMLP  = MLP( 32, [64,1], last_activation=lastActivation() )

        # Global classification
        self.globalMLP1 = MLP(nGlobalFeatures, [64,32] )
        self.globalMLP2 = MLP(32,              [64,32] )
        self.globalMLP3 = MLP(32,              [64,32] )
        self.globalMLP4 = MLP(32,              [64,1] , last_activation=lastActivation() )

        # Final score (add global and node info)  
        self.score = MLP( 2, [64, 32,1], last_activation=lastActivation() )
        
    def forward(self, nodeFeatures_raw, globalFeatures_raw, edgeI_pergraph, numNodesPerGraph, numEdgesPergraph, batchVector, edgeSendI, edgeReciI):

        # Global classification
        #print(globalFeatures_raw)
        globalFeatures = self.normGlobals(globalFeatures_raw)
        #print(self.MLP(globalFeatures))
        #sys.exit(1)
        j = self.globalMLP1(globalFeatures)
        j = self.globalMLP2(j)
        j = self.globalMLP3(j)
        j = self.globalMLP4(j)
        #print(j) 
        #sys.exit(1)

        # Node-level classification 
        nodeFeatures = self.normNodes(nodeFeatures_raw)
        edgeI        = self.shiftNI(edgeI_pergraph, numNodesPerGraph, numEdgesPergraph)

        ## Node classification
        y = self.preMLP(nodeFeatures)
        y = self.mpNodesToEdges(y, edgeI)
        y = self.mpEdgesToNodes(nodeFeatures, y, edgeI, edgeSendI, edgeReciI)
       
        ## Message passing
        #y = self.mpBlock1(y, edgeI)
        #y = self.mpBlock2(y, edgeI)
        #y = self.mpBlock3(y, edgeI)

        # Node classification
        y = self.postMLP(y)

        # Jet classification at node level
        y = global_mean_pool(y, batchVector)

        # Final score
        y = torch.cat( (y, j), 1 )
        y = self.score(y) 

        # Sum of global and node level classification
        #y = y+j/2

        return y

#############################################################################################################
# Training 
#############################################################################################################

epochList  = []
lossList   = []
lossList_v = []
accList    = []
accList_v  = []

# Trainning 
def trainingLoop(gnn, batchSize, nepochs, opti, lossFunc):
    Ntot = len(arrays_train._TrueLabel)

    # Initialize optimizer ('opti' is a function returning a pytorch optimizer)
    optimizer = opti(gnn.parameters())

    # Loop in epochs
    for epoch in range(nepochs):
        loss   = 999
        loss_v = 999
        totalCorrect = 0 
        totalSamples = 0
        totalCorrect_valid = 0 
        totalSamples_valid = 0

        #Loop in batches
        for i,b in enumerate(range(0,Ntot, batchSize)):
            stop = min(Ntot, b+batchSize)

            # Create a batch 
            #feat_b, edg_b, nnod_b, nedg_b, bVector, edgeSend_b, edgeRec_b, bWeights = batch(b, stop, tensors_train, features)
            feat_b, edg_b, nnod_b, nedg_b, bVector, edgeSend_b, edgeRec_b, globFeat_b = batch(b, stop, tensors_train, features)

            # Model prediction
            #modelPrediction = gnn(feat_b, edg_b, nnod_b, nedg_b, bVector, edgeFeats_b, edgeSend_b, edgeRec_b)
            modelPrediction = gnn(feat_b, globFeat_b, edg_b, nnod_b, nedg_b, bVector, edgeSend_b, edgeRec_b)

            # Retrieve the true label for this batch
            trueLabel = tensors_train._TrueLabel[b:stop]
            shape     = trueLabel.shape[0]

            # Calculate the accuracy
            totalCorrect += (modelPrediction[:,0].round() == trueLabel).sum().item() 
            totalSamples += shape

            # Calculate the loss
            #l = lossFunc(modelPrediction, nnod_b, trueLabel, shape, bWeights)
            l = lossFunc(modelPrediction, nnod_b, trueLabel, shape)
            l = l.mean()

            # Perform an optimization step
            optimizer.zero_grad()
            l.backward()
            optimizer.step()
            loss = l
            
            # Calculate accuracy for validation sample
            #feat_valid, edg_valid, nnod_valid, nedg_valid, vVector, edgeSend_v, edgeRec_v, vWeights = batch(0, nEvents_valid, tensors_valid, features)
            feat_valid, edg_valid, nnod_valid, nedg_valid, vVector, edgeSend_v, edgeRec_v, globFeat_valid = batch(0, nEvents_valid, tensors_valid, features)
            trueLabel_valid       = tensors_valid._TrueLabel[0:nEvents_valid]
            shape_valid           = trueLabel_valid.shape[0]
            #modelPrediction_valid = gnn(feat_valid, edg_valid, nnod_valid, nedg_valid, vVector, edgeFeats_v, edgeSend_v, edgeRec_v)
            modelPrediction_valid = gnn(feat_valid, globFeat_valid, edg_valid, nnod_valid, nedg_valid, vVector, edgeSend_v, edgeRec_v)
            totalCorrect_valid   += (modelPrediction_valid[:,0].round() == trueLabel_valid).sum().item() 
            totalSamples_valid   += shape_valid
            
            # Calculate loss for validation sample
            #l_valid = lossFunc(modelPrediction_valid, nnod_valid, trueLabel_valid, shape_valid, vWeights)
            l_valid = lossFunc(modelPrediction_valid, nnod_valid, trueLabel_valid, shape_valid)
            loss_v = l_valid

        print(f'Epoch {epoch}: Loss = %.3f | Accuracy = %.3f | Validation loss = %.3f | Validation accuracy = %.3f'%
                                    (loss, totalCorrect/totalSamples, loss_v, totalCorrect_valid/totalSamples_valid))
        lossList.  append(loss.detach().numpy())
        lossList_v.append(loss_v.detach().numpy())
        accList.   append(totalCorrect/totalSamples)
        accList_v. append(totalCorrect_valid/totalSamples_valid)
        epochList. append(epoch)

    return gnn

## Run the training (`partial` is a trick to predefine which parameters are going to be passed to the optimizer)
print(f"Run training: * nEpochs   = {nEpochs}   ")
print(f"              * batchSize = {batchSize} ")
print(f"              * learnRate = {learningRate} ")
print(f"=================================================")
gnn = GNN(tensors_all)
trainingLoop( gnn, batchSize, nEpochs, partial(torch.optim.NAdam, lr=learningRate), jetBCE )

#############################################################################################################
# Model evaluation and plotting 
#############################################################################################################

# Plot loss and accuracy
fig1, ax1 = plt.subplots(2,1)
ax1[0].plot(epochList, lossList  )
ax1[0].plot(epochList, lossList_v)
ax1[0].set_ylabel("Loss")
ax1[0].grid()

trainFig, = ax1[1].plot(epochList, accList,   label="Train sample"     )
validFig, = ax1[1].plot(epochList, accList_v, label="Validation sample")
ax1[1].set_ylabel("Accuracy")
ax1[1].set_xlabel("Number of epochs")
ax1[1].grid()
ax1[1].legend(handles=[trainFig, validFig])

## ATLAS
maxY = max(lossList+lossList_v)
ax1[0].text(0, maxY*0.98,       "ATLAS", fontsize=13, color='black', fontweight='bold', style='italic')
ax1[0].text(0, maxY*0.98,       "             simulation", fontsize=12, color='black')
ax1[0].text(0, maxY*0.96, r"$\sqrt{s}=13$TeV, H$\rightarrow$aa$\rightarrow$cccc", fontsize=9, color='black')
ax1[0].text(0, maxY*0.94, r"$m_{a}=20$GeV", fontsize=9, color='black')

plt.savefig("loss_acc.pdf")

# Plot scoring 
s_train = [] 
b_train = []
w_train = []
w_s_train = [] 
w_b_train = []

s_test  = [] 
b_test  = [] 
w_test  = [] 
w_s_test = [] 
w_b_test = []

sc_train = []
y_train  = []
sc_test  = []
y_test   = []

# Train sample
iterator = tqdm(range(nEvents_train), desc='Histogramming training set scoring')
for i in iterator:     
    #feat_train, edg_train, nnod_train, nedg_train, bVec_train, weight_train = batch(i,i+1, tensors_train, features)
    #feat_train, edg_train, nnod_train, nedg_train, bVec_train, edgeSend_train, edgeRec_train, weight_train = batch(i,i+1, tensors_train, features)
    feat_train, edg_train, nnod_train, nedg_train, bVec_train, edgeSend_train, edgeRec_train, globFeat_train = batch(i,i+1, tensors_train, features)
    score_train = gnn(feat_train, globFeat_train, edg_train, nnod_train, nedg_train, bVec_train, edgeSend_train, edgeRec_train).detach().numpy()[0][0]
    #w = weight_train.detach().numpy()

    sc_train.append(score_train)
    y_train. append(tensors_train._TrueLabel[i])
    #w_train. append(w)

    if (tensors_train._TrueLabel[i] == 0): b_train.append(score_train); #w_b_train.append(w[0])  
    if (tensors_train._TrueLabel[i] == 1): s_train.append(score_train); #w_s_train.append(w[0])

# Test sample
iterator = tqdm(range(nEvents_test), desc='Histogramming testing set scoring')
for i in iterator:     
    #feat_test, edg_test, nnod_test, nedg_test, bVec_test, edgeSend_test, edgeRec_test, weight_test = batch(i,i+1, tensors_test, features)
    feat_test, edg_test, nnod_test, nedg_test, bVec_test, edgeSend_test, edgeRec_test, globFeat_test= batch(i,i+1, tensors_test, features)
    score_test = gnn(feat_test, globFeat_test, edg_test, nnod_test, nedg_test, bVec_test, edgeSend_test, edgeRec_test).detach().numpy()[0][0]
    #w = weight_test.detach().numpy()

    sc_test.append(score_test)
    y_test. append(tensors_test._TrueLabel[i])
    #w_test. append(w)

    if (tensors_test._TrueLabel[i] == 0): b_test.append(score_test); #w_b_test.append(w[0])  
    if (tensors_test._TrueLabel[i] == 1): s_test.append(score_test); #w_s_test.append(w[0])

# Initiate plotting
fig, ax = plt.subplots()

# Binning
nbins    = 61
bins     = np.linspace(0, 1, nbins) 
binWidth = bins[1]-bins[0]

# Histograms
hist_s_test,  binEdges_s_test  = np.histogram(s_test,  density = True, bins = bins)#, weights = w_s_test)  
hist_b_test,  binEdges_b_test  = np.histogram(b_test,  density = True, bins = bins)#, weights = w_b_test)  
hist_s_train, binEdges_s_train = np.histogram(s_train, density = True, bins = bins)#, weights = w_s_train)   
hist_b_train, binEdges_b_train = np.histogram(b_train, density = True, bins = bins)#, weights = w_b_train)

# Log scale?
#hist_s_test   = np.log(hist_s_test)
#hist_b_test   = np.log(hist_b_test)
#hist_s_train  = np.log(hist_s_train)
#hist_b_train  = np.log(hist_b_train)

## Error bars
#err_s       = np.sqrt(hist_s_test /(len(s_test)*binWidth)) 
#err_b       = np.sqrt(hist_b_test /(len(b_test)*binWidth))  
#err_s_train = np.sqrt(hist_s_train/(len(s_train)*binWidth))    
#err_b_train = np.sqrt(hist_b_train/(len(b_train)*binWidth))     

# Signal test
ax.bar( bins[:-1]+(bins[1]-bins[0])/2, 
        hist_s_test, 
        width   = binWidth, 
        align   = 'center',  
        color   = 'cornflowerblue', 
        capsize = 2, 
        alpha   = 1,   
        label   = 'Dicharm jets (test)',     
        #yerr    = err_s, 
        ecolor  = 'cornflowerblue')

# Background test
ax.bar( bins[:-1]+(bins[1]-bins[0])/2, 
        hist_b_test, 
        width   = binWidth, 
        align   = 'center',                   
        color   = 'darkmagenta',    
        capsize = 2, 
        alpha   = 0.7, 
        label   = 'QCD jets (test)', 
        #yerr    = err_b, 
        ecolor  = 'darkmagenta')         

# Signal train
ax.bar( bins[:-1]+(bins[1]-bins[0])/2, 
        hist_s_train, 
        width     = binWidth, 
        align     = 'center',                   
        color     = 'none', 
        edgecolor = 'blue',
        capsize   = 2, 
        alpha     = 1, 
        label     = 'Dicharm jets (train)',    
        #yerr      = err_s_train, 
        ecolor    = 'blue')     

# Background train
ax.bar( bins[:-1]+(bins[1]-bins[0])/2, 
        hist_b_train, 
        width     = binWidth, 
        align     = 'center',                   
        color     = 'none', 
        edgecolor = 'red', 
        capsize   = 2, 
        alpha     = 1, 
        label     = 'QCD jets (train)',
        #yerr      = err_b_train, 
        ecolor    = 'red')

# Labels and legends
plt.legend(loc='upper right')
#plt.yscale('log')
plt.ylabel('N entries')
plt.xlabel('GNN score')

maxYval = max(np.concatenate((hist_s_test,hist_b_test,hist_s_train,hist_b_train), axis=0))

plt.ylim([0,maxYval*1.25])

## ATLAS
plt.text(0, maxYval*1.15, "ATLAS", fontsize=13, color='black', fontweight='bold', style='italic')
plt.text(0, maxYval*1.15, "             simulation", fontsize=12, color='black')
plt.text(0, maxYval*1.10, r"$\sqrt{s}=13$TeV, H$\rightarrow$aa$\rightarrow$cccc", fontsize=9, color='black')
plt.text(0, maxYval*1.05, r"$m_{a}=20$GeV", fontsize=9, color='black')

plt.savefig("score.pdf")

# Plot ROC curve
fpr_train, tpr_train, thresholds_train = roc_curve(y_train, sc_train)#, sample_weight = w_train)
fpr_test,  tpr_test,  thresholds_test  = roc_curve(y_test,  sc_test )#, sample_weight = w_test)

plt.figure()
testLine  = plt.plot(tpr_test, fpr_test,   label = f'Test sample ')#, AUC = {testAUC}')
trainLine = plt.plot(tpr_train, fpr_train, label = f'Train sample')#, AUC = {trainAUC}')
plt.plot([0, 1], [0, 1], linestyle="-", color="black")
plt.xlabel('Dicharm jets efficiency (True positive rate)')
plt.ylabel('QCD jets efficiency (False positive rate)')
plt.title(f'ROC curves')
plt.legend(loc='upper right')
plt.grid()

# add marker for 80% signal eff point
xvalues = testLine[0].get_xdata()
yvalues = testLine[0].get_ydata()

xval = min(xvalues, key=lambda x:abs(x-0.8))
idx  = np.where(xvalues==xval)
yval = yvalues[idx][0]

plt.scatter(xval, yval, color='r')
plt.annotate("("+str(round(xval,3))+"; "+str(round(yval,3))+")", (xval+0.02, yval))
plt.vlines(x = xval, ymin = 0, ymax = yval, colors = 'r', linestyle = 'dashed')
plt.hlines(y = yval, xmin = 0, xmax = xval, colors = 'r', linestyle = 'dashed')

## ATLAS
plt.text(0, 0.95, "ATLAS", fontsize=13, color='black', fontweight='bold', style='italic')
plt.text(0, 0.95, "             simulation", fontsize=12, color='black')
plt.text(0, 0.90, r"$\sqrt{s}=13$TeV, H$\rightarrow$aa$\rightarrow$cccc", fontsize=9, color='black')
plt.text(0, 0.85, r"$m_{a}=20$GeV", fontsize=9, color='black')

plt.savefig("rocCurve.pdf")

