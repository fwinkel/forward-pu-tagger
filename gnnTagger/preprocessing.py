import torch
import torch.nn as torch_nn
import torch.nn.functional as F
import os
import sys
import types
import uproot
import random 
import numpy as np
import awkward as ak
import matplotlib.pyplot as plt
from torch.nn import Linear
from torch_scatter import segment_coo, scatter
from torch_geometric.nn import global_mean_pool
from functools import partial
from tqdm import tqdm
from sklearn.metrics import roc_curve

#############################################################################################################
# Functions for input preprocessing
#############################################################################################################

def toNumpy(awkarray):
    """ ak.ravel will give back a flat view of the array"""
    return np.asanyarray( ak.ravel( awkarray) )
    
def toTorchTensor(awkarray):
    """ ak.ravel will give back a flat view of the array                                                   """    
    """ then we translate to a numpy array so torch can view the underlying data :                         """       
    """ (When running on CPU this is a zero copy operation, the same data is viewed by torch and awkarray) """    
    return torch.tensor( np.asanyarray( ak.ravel( awkarray) ))                                                   

def buildSCalesOffsets(features, tensors):
    n_features = len(features)
    # init empty tensors :
    scales  = torch.zeros(n_features)
    offsets = torch.zeros(n_features)
    for i,f in enumerate(features):
        arrayFeature = getattr(tensors,f)
        scales[i]    = 1./arrayFeature.std()
        offsets[i]   = -arrayFeature.mean()*scales[i]

    return scales, offsets

def toOffset( numbers ):
    """We first need to prepare a table of 'offset'. 
       For example such that for a graph g, the corresponding node features in the feature array will start at position nodeOffsets[g] until position nodeOffsets[g+1
       We can get such a table from the array of the numbers of nodes per graph (and from the fact that the features are properly stacked).
    """
    numbers = np.asanyarray(ak.ravel(numbers)) # convert to flat numpy array
    offsets = np.zeros(numbers.shape[0]+1, dtype=numbers.dtype)
    offsets[1:]  = np.cumsum(numbers)
    return offsets

def batch(start, stop, Tensors, features):
    """Function that extracts all necessary info from all the graphs between position start and position stop"""
    # node features
    n0 = Tensors.nodeOffsets[start]
    n1 = Tensors.nodeOffsets[stop ]

    # retrieve features between n0 and n1
    arrayList=[]
    for i,f in enumerate(features): arrayList.append( getattr(Tensors, f)[n0:n1] )

    # Build a compact 2d array by stacking individual features:
    # By stacking you get an array of dim(nInputs, nFeatures)
    inputs = torch.stack(arrayList, dim=1)

    # same for the edges:
    n0 = Tensors.edgeOffsets[start]
    n1 = Tensors.edgeOffsets[stop]

    # retrieve features between n0 and n1
    recieverI = Tensors.Tracks_recieverI[n0:n1]
    senderI   = Tensors.Tracks_senderI[n0:n1]

    # Build adjacency list
    edges = torch.stack([senderI, recieverI], dim=1)

    # Shift edge indexes for each track to point to it's jet
    n0 = Tensors.mpOffsets[start]
    n1 = Tensors.mpOffsets[stop ]   

    # Vector with the total number of lines per node
    bOffsets  = Tensors.NumberOfUnions[start:stop]
  
    # retrieve features between n0 and n1
    edgeSendI = Tensors.EdgeSenderI[n0:n1]
    edgeReciI = Tensors.EdgeRecieverI[n0:n1]

    # Convert to numpy array to optimize operations in for loop
    bOffsets  = np.array(bOffsets)
    edgeSendI = np.array(edgeSendI)
    edgeReciI = np.array(edgeReciI)
    
    shift, dlim, ulim = 0, 0, 0
    for offset in bOffsets:
        ulim += offset
        slice_range = slice(dlim, ulim)
        edgeSendI[slice_range] += shift
        edgeReciI[slice_range] += shift
        shift = np.max(edgeSendI[slice_range]) + 1
        dlim += offset

    # Back to torch tensor
    edgeSendI = torch.from_numpy(edgeSendI)
    edgeReciI = torch.from_numpy(edgeReciI)

    # We'll also need the number of nodes per graph
    numNodes = Tensors._nTracks[start:stop]
    # And the number of edges per graph
    numEdges = Tensors.NumberOfEdges[start:stop]
    # Event weights
    #weights = Tensors.evWeight[start:stop]

    # Jet level variables
    pt  = Tensors.jetPt [start:stop]
    eta = Tensors.jetEta[start:stop]
    jetInputs = torch.stack([pt, eta], dim=1)
 
    #print(jetInputs)
    #sys.exit(1)

    # build batch vector 
    bVector   = []
    for j,n in enumerate(numNodes): bVector+=[j]*n
    bVector = torch.tensor(bVector)  

    # return everything for this batch
    #return inputs, edges, numNodes, numEdges, bVector, edgeSendI, edgeReciI, weights
    return inputs, edges, numNodes, numEdges, bVector, edgeSendI, edgeReciI, jetInputs
