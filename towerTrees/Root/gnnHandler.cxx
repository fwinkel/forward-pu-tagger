#include <numeric>
#include "towerTrees/gnnHandler.h"
#include "PathResolver/PathResolver.h"
#include <AsgMessaging/MessageCheck.h>

using namespace std;

namespace gnnScore {

  // Constructor
  gnnScoreHandler::gnnScoreHandler(std::string m_modelName) : m_modelName(m_modelName) {
    // Use the path resolver to find the location of the network .onnx file
    m_modelPath = PathResolverFindCalibFile(m_modelName);

    // Use the default ONNX session settings for 1 CPU thread
    m_onnxSessionOptions.SetIntraOpNumThreads(1);
    m_onnxSessionOptions.SetGraphOptimizationLevel(ORT_ENABLE_BASIC);

    // Initialise the ONNX environment and session using the above options and the model name
    m_onnxSession = std::make_unique<Ort::Session>(m_onnxEnv, m_modelPath.c_str(), m_onnxSessionOptions);

    // Allocator is used to get model information
    Ort::AllocatorWithDefaultOptions allocator;

    // Get the input nodes
    size_t num_input_nodes = m_onnxSession->GetInputCount();
 
    // Iterate over all input nodes
    for (std::size_t i = 0; i < num_input_nodes; i++) {
      //auto input_name = m_onnxSession->GetInputName(i, allocator);
      const char* input_name = m_onnxSession->GetInputNameAllocated(i, allocator).release();
      m_input_node_names.push_back(input_name);
    }

    // Get the output nodes
    size_t num_output_nodes = m_onnxSession->GetOutputCount();
    std::vector<int64_t> output_node_dims;

    // Iterate over all output nodes
    for(std::size_t i = 0; i < num_output_nodes; i++ ) {
      //auto output_name = m_onnxSession->GetOutputName(i, allocator);
      const char* output_name = m_onnxSession->GetOutputNameAllocated(i, allocator).release();
      ONNXOutputNode     output_node;
      output_node.name = std::string(output_name);
      output_node.type = m_onnxSession->GetOutputTypeInfo(i).GetTensorTypeAndShapeInfo().GetElementType();
      output_node.rank = m_onnxSession->GetOutputTypeInfo(i).GetTensorTypeAndShapeInfo().GetShape().size();
      m_output_node_names.push_back(output_name);
      
      m_output_nodes.push_back(output_node);
    }

    // Check initialization
    isInitialized = true;
  }

  // Destructor
  gnnScoreHandler::~gnnScoreHandler() { }

  StatusCode gnnScoreHandler::initialize() {
    if (m_modelPath == "") return StatusCode::FAILURE;
    return StatusCode::SUCCESS;

  }

  int gnnScoreHandler::getReqSize(){
    // Returns the required size of the inputs for the network
    return (int)m_inputDims[1];

  }

  bool gnnScoreHandler::getIsInitialized(){
    return this->isInitialized;
  }

  std::vector<float> gnnScoreHandler::predict(std::map<std::string, input_pair>& gnn_inputs) {

    // This method passes a input vector through the neural network and returns its estimate. It requires conversions to onnx type tensors and back

    // Create a CPU tensor to be used as input
    auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);

    std::vector<Ort::Value> input_tensors;
    for (auto const &node_name : m_input_node_names){
      input_tensors.push_back(Ort::Value::CreateTensor<float>( memory_info, 
                                                               gnn_inputs.at(node_name).first.data(), 
                                                               gnn_inputs.at(node_name).first.size(),
                                                               gnn_inputs.at(node_name).second.data(), 
                                                               gnn_inputs.at(node_name).second.size() ) );
    }

    // Casting vector<string> to vector<const char*>. this is what ORT expects
    std::vector<const char*> input_node_names(m_input_node_names.size(), nullptr);
    for (int i=0; i<static_cast<int>(m_input_node_names.size()); i++) { input_node_names[i] = m_input_node_names.at(i).c_str(); }

    std::vector<const char*> output_node_names(m_output_nodes.size(), nullptr);
    for (int i=0; i<static_cast<int>(m_output_nodes.size()); i++) { output_node_names[i] = m_output_node_names.at(i).c_str(); }

    auto output_tensors = m_onnxSession->Run( Ort::RunOptions{nullptr},
                                              input_node_names.data(), 
                                              input_tensors.data(), 
                                              input_node_names.size(),
                                              output_node_names.data(), 
                                              output_node_names.size() );

    std::vector<float> outputs = {};

    std::map<std::string, float>              output_f;
    std::map<std::string, std::vector<char>>  output_vc;
    std::map<std::string, std::vector<float>> output_vf;
  
    for (unsigned int node_idx=0; node_idx<m_output_nodes.size(); node_idx++){

      auto tensor_type  = output_tensors.at(node_idx).GetTypeInfo().GetTensorTypeAndShapeInfo().GetElementType();
      auto tensor_shape = output_tensors.at(node_idx).GetTypeInfo().GetTensorTypeAndShapeInfo().GetShape();

      if (tensor_type == ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT){
        if (tensor_shape.size() == 0){
          output_f.insert( { m_output_nodes[node_idx].name, output_tensors.at(node_idx).GetTensorData<float>()[0] } );
          outputs.push_back(output_tensors.at(node_idx).GetTensorData<float>()[0]);
        }
        else if (tensor_shape.size() == 1){
          const float *float_ptr = output_tensors.at(node_idx).GetTensorData<float>();
          int float_ptr_len      = output_tensors[node_idx].GetTensorTypeAndShapeInfo().GetElementCount();
          output_vf.insert( { m_output_nodes[node_idx].name, {float_ptr, float_ptr + float_ptr_len} } );
          delete float_ptr;
        }
        else{ throw std::runtime_error("gnnScoreHandler::gnnScoreHandler: unsupported tensor shape"); }
      } 
      else if (tensor_type == ONNX_TENSOR_ELEMENT_DATA_TYPE_INT8){
        if (tensor_shape.size() == 1){
          const char *char_ptr = output_tensors.at(node_idx).GetTensorMutableData<char>();
          int char_ptr_len     = output_tensors[node_idx].GetTensorTypeAndShapeInfo().GetElementCount();
          output_vc.insert( { m_output_nodes[node_idx].name, {char_ptr, char_ptr + char_ptr_len} } );
          delete char_ptr;
        }
        else{ throw std::runtime_error("gnnScoreHandler::gnnScoreHandler: unsupported tensor shape"); }
      } else{ throw std::runtime_error("gnnScoreHandler::gnnScoreHandler: unsupported tensor type"); }
    }

    return outputs;

  }

}


