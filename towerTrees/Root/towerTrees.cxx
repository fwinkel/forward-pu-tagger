#include <typeinfo>

#include <AsgMessaging/MessageCheck.h>
#include <towerTrees/towerTrees.h>
//#include <towerTrees/gnnHandler.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODTruth/TruthParticle.h"

#include <JetJvtEfficiency/JetJvtEfficiency.h>

#include <fastjet/ClusterSequence.hh>

#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>

using namespace std;

towerTrees :: towerTrees (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)

{
  //declareProperty( "JetPtCut", m_jetPtCut = 25000.0, "Minimum jet pT (in MeV)" );

}


StatusCode towerTrees :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  ANA_MSG_INFO( "=======================");
  ANA_MSG_INFO( "  Calling fileExecute  ");
  ANA_MSG_INFO( "=======================");

  // get TEvent and TStore - must be done here b/c we need to retrieve CutBookkeepers container from TEvent!
  
  xAOD::TEvent *m_event = wk()->xaodEvent();
  xAOD::TStore *m_store = wk()->xaodStore();

  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////// MetaData ////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////

  // get the MetaData tree once a new file is opened, with
 
  TTree* MetaData = dynamic_cast<TTree*>( wk()->inputFile()->Get("MetaData") );
  if ( !MetaData ) {
    ANA_MSG_ERROR( "MetaData tree not found! Exiting.");
    return StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);
  
  //---------------------------
  // Meta data - CutBookkepers
  //---------------------------
  // Metadata for intial N (weighted) events are used to correctly normalise MC
  // if running on a MC DAOD which had some skimming applied at the derivation stage

  // Now, let's find the actual information
  const xAOD::CutBookkeeperContainer* completeCBC(nullptr);
  if ( !m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess() ) {
      ANA_MSG_ERROR("Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      return StatusCode::FAILURE;
  }

  // Find the smallest cycle number, the original first processing step/cycle
  int minCycle(10000);
  for ( auto cbk : *completeCBC ) {
      if ( !( cbk->name().empty() )  && ( minCycle > cbk->cycle() ) ){ minCycle = cbk->cycle(); }
  }

  // Now, let's actually find the right one that contains all the needed info...
  const xAOD::CutBookkeeper* allEventsCBK(nullptr);
  const xAOD::CutBookkeeper* DxAODEventsCBK(nullptr);

  bool m_isDerivation = true;
  TString m_derivationName = "JETM1Kernel";
  //TString m_derivationName = "PHYSKernel";
  //TString m_derivationName = "FTAG1Kernel";

  int maxCycle(-1);
  for ( const xAOD::CutBookkeeper* cbk: *completeCBC ){
    // Find initial book keeper
    ANA_MSG_INFO("Complete cbk name: " << cbk->name() << " - stream: " << cbk->inputStream() );
    if( cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" ){
        allEventsCBK = cbk;
        maxCycle = cbk->cycle();
    }
    // Find derivation book keeper
    if ( m_isDerivation ){
      if(m_derivationName != "")
	{
	  if ( cbk->name() == m_derivationName )
	    DxAODEventsCBK = cbk;
	}
      else if( cbk->name().find("Kernel") != std::string::npos )
	{
	  ANA_MSG_INFO("Auto config found DAOD made by Derivation Algorithm: " << cbk->name());
	  DxAODEventsCBK = cbk;
	}
    } // is derivation

    // Find and record AOD-level sumW information for all alternate weights
    //  The nominal AllExecutedEvents will be filled later, due to posibility of multiple CBK entries
    if(cbk->name().substr(0,37) == "AllExecutedEvents_NonNominalMCWeight_" && cbk->name().length()!=17 && cbk->inputStream() == "StreamAOD"){
      // Extract weight index from name
      int32_t idx=std::stoi(cbk->name().substr(37));

      // Fill histogram, making sure that there is space
      // Note will fill bin index = idx+1
      while(idx>=m_histSumW->GetNbinsX())
        m_histSumW->LabelsInflate("X");
      m_histSumW->Fill(idx, cbk->sumOfEventWeights());
    }
  }
  
  if(allEventsCBK == nullptr) {
    ANA_MSG_WARNING("No allEventsCBK found (this is expected for DataScouting, otherwise not). Event numbers set to 0.");
    m_MD_initialNevents     = 0;
    m_MD_initialSumW        = 0;
    m_MD_initialSumWSquared = 0;
  }
  else {
    m_MD_initialNevents     = allEventsCBK->nAcceptedEvents();
    m_MD_initialSumW        = allEventsCBK->sumOfEventWeights();
    m_MD_initialSumWSquared = allEventsCBK->sumOfEventWeightsSquared();
  }

  m_MD_finalNevents	= DxAODEventsCBK->nAcceptedEvents(); 
  m_MD_finalSumW	= DxAODEventsCBK->sumOfEventWeights(); 
  m_MD_finalSumWSquared = DxAODEventsCBK->sumOfEventWeightsSquared();

  // Write metadata event bookkeepers to histogram
  ANA_MSG_INFO( "Meta data from this file:");
  ANA_MSG_INFO( "Initial  events  = "                << static_cast<unsigned int>(m_MD_initialNevents) );
  ANA_MSG_INFO( "Selected events  = "                << static_cast<unsigned int>(m_MD_finalNevents) );
  ANA_MSG_INFO( "Initial  sum of weights = "         << m_MD_initialSumW);
  ANA_MSG_INFO( "Selected sum of weights = "         << m_MD_finalSumW);
  ANA_MSG_INFO( "Initial  sum of weights squared = " << m_MD_initialSumWSquared);
  ANA_MSG_INFO( "Selected sum of weights squared = " << m_MD_finalSumWSquared);

  //hist("MetaData_SumW")->Fill(0., m_MD_initialSumW);
  //m_histSumW->Fill(0., m_MD_initialSumW);
  //m_cutflowHist ->Fill(m_cutflow_all, m_MD_initialNevents);
  //m_cutflowHistW->Fill(m_cutflow_all, m_MD_initialSumW);

  hist("MetaData_EventCount") -> Fill(1, m_MD_initialNevents);
  hist("MetaData_EventCount") -> Fill(2, m_MD_finalNevents);
  hist("MetaData_EventCount") -> Fill(3, m_MD_initialSumW);
  hist("MetaData_EventCount") -> Fill(4, m_MD_finalSumW);
  hist("MetaData_EventCount") -> Fill(5, m_MD_initialSumWSquared);
  hist("MetaData_EventCount") -> Fill(6, m_MD_finalSumWSquared);

  return StatusCode::SUCCESS;
}

StatusCode towerTrees :: initialize ()
{
    
  ANA_MSG_INFO ("in initialize");

  // Booking histograms
  ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]
  ANA_CHECK (book (TH1F ("h_jetEta", "h_jetEta", 100, -6, 6)));
  ANA_CHECK (book (TH1F ("cutflow", "cutflow", 11, 0, 11)));
  ANA_CHECK (book (TH1F ("MetaData_SumW", "MetaData_SumW", 1, -0.5, 0.5)));
  hist("MetaData_SumW")->SetCanExtend(TH1::kAllAxes);

  ANA_CHECK (book (TH1F ("MetaData_EventCount", "MetaData_EventCount", 6, 0.5, 6.5)));
  hist("MetaData_EventCount")->GetXaxis()->SetBinLabel(1, "nEvents initial");
  hist("MetaData_EventCount")->GetXaxis()->SetBinLabel(2, "nEvents selected");
  hist("MetaData_EventCount")->GetXaxis()->SetBinLabel(3, "sumOfWeights initial");
  hist("MetaData_EventCount")->GetXaxis()->SetBinLabel(4, "sumOfWeights selected");
  hist("MetaData_EventCount")->GetXaxis()->SetBinLabel(5, "sumOfWeightsSquared initial");
  hist("MetaData_EventCount")->GetXaxis()->SetBinLabel(6, "sumOfWeightsSquared selected");

//  m_histEventCount = new TH1D("MetaData_EventCount", "MetaData_EventCount", 6, 0.5, 6.5);
//  m_histEventCount->GetXaxis()->SetBinLabel(1, "nEvents initial");
//  m_histEventCount->GetXaxis()->SetBinLabel(2, "nEvents selected");
//  m_histEventCount->GetXaxis()->SetBinLabel(3, "sumOfWeights initial");
//  m_histEventCount->GetXaxis()->SetBinLabel(4, "sumOfWeights selected");
//  m_histEventCount->GetXaxis()->SetBinLabel(5, "sumOfWeightsSquared initial");
//  m_histEventCount->GetXaxis()->SetBinLabel(6, "sumOfWeightsSquared selected");

  // Get metadata
  ANA_CHECK( requestFileExecute () );

  // Event cuts
  const char *eventCuts[11] = {"All events", 
                               "Events passed trigger", 
                               "Existent Vertex",
                               "Events with at least two muons",
                               "Events with #mu^{-} #mu^{+} pair",
                               "Events passed #mu TTVA",
                               "Events with m_{ll} #neq m_{Z}", 
                               "Events with at least 2 jets", 
                               "Events passed JVT", 
                               "Events passed #eta and pT cuts for jets", 
                               "-"};
 
  for (int i=0; i<11; ++i ){hist("cutflow")->Fill(eventCuts[i],0);}

  cutflow_all      = 0;
  cutflow_trigger  = 0;
  cutflow_vertex   = 0;
  cutflow_2muons   = 0;
  cutflow_mupair   = 0;
  cutflow_muttva   = 0;
  cutflow_mz       = 0;
  cutflow_2jets    = 0;
  cutflow_jvt      = 0;
  cutflow_jetPtEta = 0;
  cutflow_final    = 0;

  // Booking tree
  ANA_CHECK (book (TTree ("towerTree", "My analysis ntuple")));
  TTree* mytree = tree ("towerTree");

  // Define vectors
  // event level info
  //m_eventWeights = new std::vector<float>();

  // Reco Jets 
  m_jetEta      = new std::vector<float>();
  m_jetPhi      = new std::vector<float>();
  m_jetPt       = new std::vector<float>();
  m_jetE        = new std::vector<float>();
  m_jetPx       = new std::vector<float>();
  m_jetPy       = new std::vector<float>();
  m_jetPz       = new std::vector<float>();
  m_jetM        = new std::vector<float>();
  m_jetTruthId  = new std::vector<int>();

  m_jetIsHS      = new std::vector<bool>();
  m_jetIsPU      = new std::vector<bool>();
  m_jetIsQCDPU   = new std::vector<bool>();
  m_jetIsStochPU = new std::vector<bool>();

  // Truth Jets 
  m_tjetEta     = new std::vector<float>();
  m_tjetPhi     = new std::vector<float>();
  m_tjetPt      = new std::vector<float>();
  m_tjetE       = new std::vector<float>();
  m_tjetPdgid   = new std::vector<float>();
  m_tjetPx      = new std::vector<float>();
  m_tjetPy      = new std::vector<float>();
  m_tjetPz      = new std::vector<float>();
  m_tjetM       = new std::vector<float>();

  m_TowerCount  = new std::vector<int>();

  m_time_tow         = new std::vector<std::vector<float>>();    
  m_eta_tow          = new std::vector<std::vector<float>>();    
  m_phi_tow          = new std::vector<std::vector<float>>();    
  m_clusterSize_tow  = new std::vector<std::vector<float>>();
  m_calE_tow         = new std::vector<std::vector<float>>();
  m_calEta_tow       = new std::vector<std::vector<float>>();
  m_calPhi_tow       = new std::vector<std::vector<float>>();
  m_rawE_tow         = new std::vector<std::vector<float>>();
  m_rawEta_tow       = new std::vector<std::vector<float>>();
  m_rawPhi_tow       = new std::vector<std::vector<float>>();
  m_rawM_tow         = new std::vector<std::vector<float>>();
  m_altM_tow         = new std::vector<std::vector<float>>();
  m_calM_tow         = new std::vector<std::vector<float>>();
  m_e_sampl_tow      = new std::vector<std::vector<float>>();
  m_longitudinal_tow = new std::vector<std::vector<float>>();
  m_eng_frac_em_tow  = new std::vector<std::vector<float>>();
  m_significance_tow = new std::vector<std::vector<float>>();
  
  // Branches
  mytree->Branch("RunNumber",                &m_runNumber                     );
  mytree->Branch("EventNumber",              &m_eventNumber                   );  
  mytree->Branch("channelNumber",            &m_channelNumber                 ); 
  mytree->Branch("averageInteractionsPerBC", &m_averageInteractionsPerCrossing); 
  mytree->Branch("actualInteractionsPerBC",  &m_actualInteractionsPerCrossing ); 
  mytree->Branch("MCEventWeight",            &m_eventWeight                   ); 

  mytree->Branch("CaloJet_eta",              &m_jetEta                        );
  mytree->Branch("CaloJet_phi",              &m_jetPhi                        );
  mytree->Branch("CaloJet_pt",               &m_jetPt                         );
  mytree->Branch("CaloJet_e",                &m_jetE                          );
  mytree->Branch("CaloJet_px",               &m_jetPx                         );
  mytree->Branch("CaloJet_py",               &m_jetPy                         );
  mytree->Branch("CaloJet_pz",               &m_jetPz                         );
  mytree->Branch("CaloJet_m",                &m_jetM                          );
  mytree->Branch("CaloJet_TruthID",          &m_jetTruthId                    );

  mytree->Branch("CaloJet_IsHS",             &m_jetIsHS                       );
  mytree->Branch("CaloJet_IsPU",             &m_jetIsPU                       );
  mytree->Branch("CaloJet_IsQCDPU",          &m_jetIsQCDPU                    );
  mytree->Branch("CaloJet_IsStochPU",        &m_jetIsStochPU                  );

  mytree->Branch("TJet_eta",                 &m_tjetEta                       );
  mytree->Branch("TJet_phi",                 &m_tjetPhi                       );
  mytree->Branch("TJet_pt",                  &m_tjetPt                        );
  mytree->Branch("TJet_e",                   &m_tjetE                         );
  mytree->Branch("TJet_PDGID",               &m_tjetPdgid                     );
  mytree->Branch("TJet_px",                  &m_tjetPx                        );
  mytree->Branch("TJet_py",                  &m_tjetPy                        );
  mytree->Branch("TJet_pz",                  &m_tjetPz                        );
  mytree->Branch("TJet_m",                   &m_tjetM                         );

  mytree->Branch("nTowers",                  &m_TowerCount                    );
  mytree->Branch("Towers_time",              &m_time_tow                      );    
  mytree->Branch("Towers_eta",               &m_eta_tow                       );    
  mytree->Branch("Towers_phi",               &m_phi_tow                       );    
  mytree->Branch("Towers_clusterSize",       &m_clusterSize_tow               );   
  mytree->Branch("Towers_calE",              &m_calE_tow                      );
  mytree->Branch("Towers_calEta",            &m_calEta_tow                    );
  mytree->Branch("Towers_calPhi",            &m_calPhi_tow                    );
  mytree->Branch("Towers_rawE",              &m_rawE_tow                      );
  mytree->Branch("Towers_rawEta",            &m_rawEta_tow                    );
  mytree->Branch("Towers_rawPhi",            &m_rawPhi_tow                    );
  mytree->Branch("Towers_rawM",              &m_rawM_tow                      );
  mytree->Branch("Towers_e_sampl",           &m_e_sampl_tow                   );
  mytree->Branch("Towers_longitudinal",      &m_longitudinal_tow              );
  mytree->Branch("Towers_eng_frac_em",       &m_eng_frac_em_tow               );
  mytree->Branch("Towers_significance",      &m_significance_tow              );

  int count = 0;

  return StatusCode::SUCCESS;
}


StatusCode towerTrees :: execute ()
{

  cutflow_all+=1;

  count += 1;
  if(count%1000==0){cout<<"---Event "<<count<<endl;} 

  // clear everything

  // Jets
  m_jetEta          ->clear();
  m_jetPhi          ->clear();
  m_jetPt           ->clear();
  m_jetE            ->clear();
  m_jetPx           ->clear();
  m_jetPy           ->clear();
  m_jetPz           ->clear();
  m_jetM            ->clear();
  m_jetTruthId      ->clear();

  m_jetIsHS         ->clear();
  m_jetIsPU         ->clear();
  m_jetIsQCDPU      ->clear();
  m_jetIsStochPU    ->clear();

  m_tjetEta         ->clear();
  m_tjetPhi         ->clear();
  m_tjetPt          ->clear();
  m_tjetE           ->clear();
  m_tjetPdgid       ->clear();
  m_tjetPdgid       ->clear();
  m_tjetPx          ->clear();
  m_tjetPy          ->clear();
  m_tjetPz          ->clear();
  m_tjetM           ->clear();
 
  // Towers 
  m_TowerCount      ->clear();
  m_time_tow        ->clear();
  m_eta_tow         ->clear();
  m_phi_tow         ->clear();
  m_clusterSize_tow ->clear();
  m_calE_tow        ->clear();
  m_calEta_tow      ->clear();
  m_calPhi_tow      ->clear();
  m_rawE_tow        ->clear();
  m_rawEta_tow      ->clear();
  m_rawPhi_tow      ->clear();
  m_rawM_tow        ->clear();
  m_e_sampl_tow     ->clear();
  m_longitudinal_tow->clear();
  m_eng_frac_em_tow ->clear();
  m_significance_tow->clear();

  // Read the EventInfo variables:
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  //////////////////////////////////////////////////////////////////////////////////////
  ////////////////// JETS ////////////////////////////////////////////////////////////// 
  //////////////////////////////////////////////////////////////////////////////////////

  bool  m_isMC = true;
  float m_truthDR  = 0.3; //hs truth jets must have pT>10GeV 
  float m_truthDR2 = 0.6; //pu truth jets must have pT>10GeV 
   
  const xAOD::JetContainer* truthJets = 0;
  ANA_CHECK (evtStore()->retrieve (truthJets, "AntiKt4TruthJets"));

  const xAOD::JetContainer* recoJets = 0;
  ANA_CHECK (evtStore()->retrieve (recoJets, "AntiKt4EMPFlowJets"));

  const xAOD::JetContainer* m_inTime_truthJets = 0;
  ANA_CHECK( evtStore()->retrieve(m_inTime_truthJets, "InTimeAntiKt4TruthJets"   ));

  const xAOD::JetContainer* m_outOfTime_truthJets = 0;
  ANA_CHECK( evtStore()->retrieve(m_outOfTime_truthJets, "OutOfTimeAntiKt4TruthJets"));

  vector< const xAOD::Jet*> truthJetVec;
  vector< const xAOD::Jet*> inTime_truthJetVec;   
  vector< const xAOD::Jet*> outOfTime_truthJetVec;

  // Get a vector of valid truth jets
  for( const xAOD::Jet* thisJet : *truthJets ) {
    if( thisJet->e()< 0          ) continue;
    if( fabs(thisJet->pt()) < 7  ) continue;
    if( fabs(thisJet->eta()) > 5 ) continue;
    truthJetVec.push_back( thisJet );
  }
  for( const xAOD::Jet* thisJet : *m_inTime_truthJets ) {                        
    if( thisJet->e()< 0          ) continue;
    if( fabs(thisJet->pt()) < 7  ) continue;
    if( fabs(thisJet->eta()) > 5 ) continue;
    inTime_truthJetVec.push_back( thisJet );                        
  }                                                                  
  for( const xAOD::Jet* thisJet : *m_outOfTime_truthJets ) {                     
    if( thisJet->e()< 0          ) continue;   
    if( fabs(thisJet->pt()) < 7  ) continue;   
    if( fabs(thisJet->eta()) > 5 ) continue;   
    outOfTime_truthJetVec.push_back( thisJet );                     
   }


  // At least two reco jets in the event
  int nJets  = recoJets->size();
  if ( nJets < 2 ){ return StatusCode::SUCCESS; }

  // pT and eta cuts
  vector<const xAOD::Jet*> preselectedJets;
  for (const xAOD::Jet* jet : *recoJets) {
    if(jet->pt()>20000 and abs(jet->eta())>4.0) { preselectedJets.push_back(jet); } 
  }

  // Require at least one forward jet per event
  if (preselectedJets.size()<1){ return StatusCode::SUCCESS; }

  // Need to implement:
  // 1. Truth matching
  // 2. PU jet definitions

  bool isHS = false;
  bool isPU = false;
  bool isQCDPU   = false;
  bool isStochPU = true; 

  vector<bool> jetIsHS;
  vector<bool> jetIsPU;
  vector<bool> jetIsQCDPU;
  vector<bool> jetIsStochPU;

  vector<const xAOD::Jet*> selectedJets;
  vector<const xAOD::Jet*> selectedTJets;

  for( const xAOD::Jet* jet : preselectedJets ){  

    const xAOD::Jet* matchedTruthJet = nullptr;

    if(m_isMC){

      // Find truth jet with smaller dR(tjet, rjet)
      for( const xAOD::Jet* truthJet : truthJetVec ){
        if( !matchedTruthJet ){
          matchedTruthJet = truthJet;
        } 
        else if( jet->p4().DeltaR( truthJet->p4() ) < jet->p4().DeltaR( matchedTruthJet->p4() ) ) { 
          matchedTruthJet = truthJet;
        }
      }

      // Find out if is HS or PU depending on dR(tjet, rjet) 
      ANA_MSG_DEBUG("Check dR truth jet matching requirement");
      if( jet->p4().DeltaR( matchedTruthJet->p4() ) < m_truthDR){
        ANA_MSG_DEBUG("Matched truth jet");
        jetIsHS     .push_back(true);
        jetIsPU     .push_back(false);
        jetIsQCDPU  .push_back(false);       
        jetIsStochPU.push_back(false);

	isHS = true;
      } 
      else if( jet->p4().DeltaR( matchedTruthJet->p4() ) > m_truthDR2 ) {
        ANA_MSG_DEBUG("No matched truth jet");
        jetIsHS.push_back(false);
        jetIsPU.push_back(true);
	isPU = true;
      }

      if (isPU == true){                                                                            
        //Need to check both containers                                                             
        for(auto itpujet : inTime_truthJetVec){                                                     
          if (itpujet->p4().DeltaR(jet->p4())<0.3 && itpujet->pt()>10e3){ isQCDPU   = true;   }       
          if (itpujet->p4().DeltaR(jet->p4())<0.6 && itpujet->pt()>10e3){ isStochPU = false;  }      
        }
        for(auto ootpujet : outOfTime_truthJetVec){                                           
          if (ootpujet->p4().DeltaR(jet->p4())<0.3 && ootpujet->pt()>10e3){ isQCDPU   = true; }     
          if (ootpujet->p4().DeltaR(jet->p4())<0.6 && ootpujet->pt()>10e3){ isStochPU = false;}     
        }
      }                                                                                             
      else {                                                                                        
        isStochPU = false; //Can't be labelled stochastic PU if jet is not determined to be PU     
      }

      jetIsQCDPU.push_back  (isQCDPU  );       
      jetIsStochPU.push_back(isStochPU);

      if ( (jet->p4().DeltaR( matchedTruthJet->p4() ) < m_truthDR) or (jet->p4().DeltaR( matchedTruthJet->p4() ) > m_truthDR2) ){
        selectedJets .push_back(jet);
        selectedTJets.push_back(matchedTruthJet);
      }
 
    }//isMC

    // Towers
    int nTowers = 0;

    // Vectors for tower variables
    vector<float> time_tow;     
    vector<float> eta_tow;     
    vector<float> phi_tow;     
    vector<float> clusterSize_tow;   
    vector<float> calE_tow; 
    vector<float> calEta_tow; 
    vector<float> calPhi_tow; 
    vector<float> rawE_tow; 
    vector<float> rawEta_tow; 
    vector<float> rawPhi_tow; 
    vector<float> rawM_tow; 
    vector<float> altM_tow; 
    vector<float> calM_tow; 
    vector<float> e_sampl_tow; 
    vector<float> eng_frac_em_tow;
    vector<float> longitudinal_tow;
    vector<float> significance_tow;

    double lon;
    double engFrac;
    double sig;

    static SG::AuxElement::ConstAccessor< vector<ElementLink<DataVector<xAOD::IParticle> > > >towersForTagging ("GhostTower");
    if (towersForTagging.isAvailable(*jet)){
      // Vector of towers linked to jets
      vector<ElementLink<DataVector<xAOD::IParticle> > > towerLinks = towersForTagging( *jet );
      // Loop in towers
      for ( auto link_itr : towerLinks ) {
        if( !link_itr.isValid() ) { continue; }
        const xAOD::CaloCluster* tower = dynamic_cast<const xAOD::CaloCluster*>( *link_itr );
        
        //// Tower cuts 
        //if (tower->pt()<500)        {continue;}
        //if (abs(tower->eta())>2.5)  {continue;}
        //if (d0>5)                   {continue;}
        //if (tower->z0()>5)          {continue;}

        // Push tower info to trees
        time_tow        .push_back( tower->time()                       );
        eta_tow         .push_back( tower->eta0()                       );
        phi_tow         .push_back( tower->phi0()                       );
        clusterSize_tow .push_back( tower->clusterSize()                );
        calE_tow        .push_back( tower->calE()                       );
        calEta_tow      .push_back( tower->calEta()                     );
        calPhi_tow      .push_back( tower->calPhi()                     );
        rawE_tow        .push_back( tower->rawE()                       );
        rawEta_tow      .push_back( tower->rawEta()                     );
        rawPhi_tow      .push_back( tower->rawPhi()                     );
        rawM_tow        .push_back( tower->rawM()                       );
        altM_tow        .push_back( tower->altM()                       );
        calM_tow        .push_back( tower->calM()                       );
        e_sampl_tow     .push_back( tower->eSample(CaloSampling::FCAL1) );

        if(tower->retrieveMoment(xAOD::CaloCluster::LONGITUDINAL, lon))    { longitudinal_tow.push_back( lon     ); }
        if(tower->retrieveMoment(xAOD::CaloCluster::ENG_FRAC_EM,  engFrac)){ eng_frac_em_tow. push_back( engFrac ); } 
        if(tower->retrieveMoment(xAOD::CaloCluster::SIGNIFICANCE, sig))    { significance_tow.push_back( sig     ); }
        
        nTowers+=1;
      }// End of loop in towers  
    }// End of if (check of link validity)

    m_time_tow        ->push_back( time_tow         );     
    m_eta_tow         ->push_back( eta_tow          );    
    m_phi_tow         ->push_back( phi_tow          );    
    m_clusterSize_tow ->push_back( clusterSize_tow  );      
    m_calE_tow        ->push_back( calE_tow         );     
    m_calEta_tow      ->push_back( calEta_tow       );         
    m_calPhi_tow      ->push_back( calPhi_tow       );          
    m_rawE_tow        ->push_back( rawE_tow         );        
    m_rawEta_tow      ->push_back( rawEta_tow       );          
    m_rawPhi_tow      ->push_back( rawPhi_tow       );      
    m_rawM_tow        ->push_back( rawM_tow         );   
    m_altM_tow        ->push_back( altM_tow         );    
    m_calM_tow        ->push_back( calM_tow         );    
    m_e_sampl_tow     ->push_back( e_sampl_tow      );   
    m_longitudinal_tow->push_back( longitudinal_tow );   
    m_eng_frac_em_tow ->push_back( eng_frac_em_tow  );  
    m_significance_tow->push_back( significance_tow );  

    m_TowerCount      ->push_back( nTowers     );

  }// End of loop in jets 

  ////////////////////////////////////////////////////////////////////////  
  // If the event got here, it passed every cut and will be selected :) //
  ////////////////////////////////////////////////////////////////////////

  cutflow_final+=1;

  // Fill event-level variables
  m_runNumber                      = eventInfo->runNumber   ();
  m_eventNumber                    = eventInfo->eventNumber ();
  m_channelNumber                  = eventInfo->mcChannelNumber ();
  m_averageInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing();    
  m_actualInteractionsPerCrossing  = eventInfo->actualInteractionsPerCrossing();  
  if ( eventInfo->mcEventWeights().size() > 0 ) {m_eventWeight = eventInfo->mcEventWeights().at(0);}

  // Fill reco jet variables
  for (long unsigned int j=0; j<selectedJets.size(); j++) {
    m_jetEta      ->push_back (selectedJets.at(j)->eta());
    m_jetPhi      ->push_back (selectedJets.at(j)->phi());
    m_jetPt       ->push_back (selectedJets.at(j)->pt ()/1e3);
    m_jetE        ->push_back (selectedJets.at(j)->e  ()/1e3);
    m_jetPx       ->push_back (selectedJets.at(j)->px ()/1e3);
    m_jetPy       ->push_back (selectedJets.at(j)->py ()/1e3);
    m_jetPz       ->push_back (selectedJets.at(j)->pz ()/1e3);
    m_jetM        ->push_back (selectedJets.at(j)->m  ()/1e3);
    m_jetTruthId  ->push_back (selectedJets.at(j)->auxdata<int>("HadronConeExclExtendedTruthLabelID"));
    m_jetIsHS     ->push_back (jetIsHS.at(j));
    m_jetIsPU     ->push_back (jetIsPU.at(j));
    m_jetIsQCDPU  ->push_back (jetIsQCDPU.at(j));
    m_jetIsStochPU->push_back (jetIsStochPU.at(j));
  }

  //// Fill truth jet variables
  for (long unsigned int j=0; j<truthJets->size(); j++) {
    m_tjetEta  ->push_back (truthJets->at(j)->eta ());
    m_tjetPhi  ->push_back (truthJets->at(j)->phi ());
    m_tjetPt   ->push_back (truthJets->at(j)->pt  ()/1e3);
    m_tjetE    ->push_back (truthJets->at(j)->e   ()/1e3);
    m_tjetPdgid->push_back (truthJets->at(j)->auxdata<int>("PartonTruthLabelID"));
  }

  // Fill the event into the tree:
  tree ("towerTree")->Fill ();

  //hist("MetaData_EventCount") -> Fill(1, 1);
  //hist("MetaData_EventCount") -> Fill(2, 1);
  //hist("MetaData_EventCount") -> Fill(3, m_eventWeight);
  //hist("MetaData_EventCount") -> Fill(4, m_eventWeight);
  //hist("MetaData_EventCount") -> Fill(5, m_eventWeight*m_eventWeight);
  //hist("MetaData_EventCount") -> Fill(6, m_eventWeight*m_eventWeight);

  return StatusCode::SUCCESS;
}



StatusCode towerTrees :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.


  return StatusCode::SUCCESS;
}

