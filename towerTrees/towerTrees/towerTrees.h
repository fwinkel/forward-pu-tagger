#ifndef MyAnalysis_towerTrees_H
#define MyAnalysis_towerTrees_H

#include <iostream>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <TTree.h>
#include <vector>
#include <AsgTools/AnaToolHandle.h>
#include <JetAnalysisInterfaces/IJetJvtEfficiency.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <ParticleJetTools/JetParticleAssociation.h>
#include <EventLoop/IWorker.h>
#include <EventLoop/Algorithm.h>
#include <towerTrees/gnnHandler.h>

// ONNX Library
#include <core/session/onnxruntime_cxx_api.h>

class towerTrees : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  towerTrees (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode fileExecute () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ~towerTrees () {
    delete m_jetEta;
    delete m_jetPhi;
    delete m_jetPt;
    delete m_jetE;

  }

private:
  // Configuration, and any other types of variables go here.

  /// Cutflow hist
  const char *analysisCuts[11];
  TH1F* cutflow    = nullptr;  //!
  TH1D* m_histSumW = nullptr;
  TH1D* m_histEventCount = nullptr; 

  int cutflow_all;           //!
  int cutflow_trigger;
  int cutflow_vertex;
  int cutflow_2muons;
  int cutflow_mupair;
  int cutflow_muttva;
  int cutflow_mz;
  int cutflow_2jets;
  int cutflow_jvt;
  int cutflow_jetPtEta;
  int cutflow_final;

  //sum of weights
  float sumOfWeights;
  float m_MD_initialNevents    ;
  float m_MD_initialSumW       ;
  float m_MD_initialSumWSquared;
  float m_MD_finalNevents      ;  
  float m_MD_finalSumW         ;  
  float m_MD_finalSumWSquared  ;  

  double m_jetPtCut;
  double m_leadJetsPtCut;

  /// Sample name
  std::string m_sampleName;

  /// Reclustering size
  double m_deltaR;

  /// event counter
  int count = 0;

  /// output variables for the current event
  unsigned int       m_runNumber                      = 0; ///< Run number
  unsigned int       m_channelNumber                  = 0; ///< MC channel number
  unsigned long long m_eventNumber                    = 0; ///< Event number
  float              m_eventWeight                    = 0;
  float              m_averageInteractionsPerCrossing = 0; 
  float              m_actualInteractionsPerCrossing  = 0; 

  /// Reco jet 4-momentum variables
  std::vector<float> *m_jetEta = nullptr;
  std::vector<float> *m_jetPhi = nullptr;
  std::vector<float> *m_jetPt  = nullptr;
  std::vector<float> *m_jetE   = nullptr;
  std::vector<float> *m_jetPx = nullptr;
  std::vector<float> *m_jetPy = nullptr;
  std::vector<float> *m_jetPz = nullptr;
  std::vector<float> *m_jetM  = nullptr;
  std::vector<int>   *m_jetTruthId  = nullptr;
  
  std::vector<bool> *m_jetIsHS      = nullptr;
  std::vector<bool> *m_jetIsPU      = nullptr;
  std::vector<bool> *m_jetIsQCDPU   = nullptr;
  std::vector<bool> *m_jetIsStochPU = nullptr;

  /// Truth jet 4-momentum variables
  std::vector<float> *m_tjetEta = nullptr;
  std::vector<float> *m_tjetPhi = nullptr;
  std::vector<float> *m_tjetPt  = nullptr;
  std::vector<float> *m_tjetE   = nullptr;
  std::vector<float> *m_tjetPdgid  = nullptr;
  std::vector<float> *m_tjetPx = nullptr;
  std::vector<float> *m_tjetPy = nullptr;
  std::vector<float> *m_tjetPz = nullptr;
  std::vector<float> *m_tjetM  = nullptr;


  /// Tower variables for training

  std::vector <std::vector<float>> *m_time_tow         = nullptr;
  std::vector <std::vector<float>> *m_eta_tow          = nullptr;
  std::vector <std::vector<float>> *m_phi_tow          = nullptr;
  std::vector <std::vector<float>> *m_clusterSize_tow  = nullptr;
  std::vector <std::vector<float>> *m_calE_tow         = nullptr;
  std::vector <std::vector<float>> *m_calEta_tow       = nullptr;
  std::vector <std::vector<float>> *m_calPhi_tow       = nullptr;
  std::vector <std::vector<float>> *m_rawE_tow         = nullptr;
  std::vector <std::vector<float>> *m_rawEta_tow       = nullptr;
  std::vector <std::vector<float>> *m_rawPhi_tow       = nullptr;
  std::vector <std::vector<float>> *m_rawM_tow         = nullptr;
  std::vector <std::vector<float>> *m_altM_tow         = nullptr;
  std::vector <std::vector<float>> *m_calM_tow         = nullptr;
  std::vector <std::vector<float>> *m_e_sampl_tow      = nullptr;
  std::vector <std::vector<float>> *m_longitudinal_tow = nullptr;
  std::vector <std::vector<float>> *m_eng_frac_em_tow  = nullptr;
  std::vector <std::vector<float>> *m_significance_tow = nullptr;
 
  std::vector<int>                  *m_TowerCount = nullptr;

  std::unique_ptr<gnnScore::gnnScoreHandler> m_gnnhandler;//!

  std::string m_NetLocation = "";

  /// Tools
  asg::AnaToolHandle<CP::IJetJvtEfficiency>      m_JVT_tool_handle{"CP::IJetJvtEfficiency/JVT"};//!
  asg::AnaToolHandle<TrigConf::ITrigConfigTool>  m_trigConfTool_handle {"TrigConf::xAODConfigTool/xAODConfigTool", this}; //!
  asg::AnaToolHandle<Trig::TrigDecisionTool>     m_trigDecTool_handle{"Trig::TrigDecisionTool/TrigDecisionTool"}; //!


protected:
  // This if for getting some tower-related objects via element link
  template<typename T, typename U, typename V> void safeFill(const V* xAODObj, SG::AuxElement::ConstAccessor<T>& accessor, std::vector<U>* destination, U defaultValue, int units = 1){
    if ( accessor.isAvailable( *xAODObj ) ) {
      destination->push_back( accessor( *xAODObj ) / units );
    } else {
      destination->push_back( defaultValue );
    }
  }

};

#endif
