#ifndef MYANALYSIS_GNNSCORE_DICT_H
#define MYANALYSIS_GNNSCORE_DICT_H

// STL includes
#include <string>

// Asg tool includes
#include "AsgTools/AsgTool.h"
#include "AsgTools/AnaToolHandle.h"
#include <AsgMessaging/MessageCheck.h>

// ONNX Library
#include <core/session/onnxruntime_cxx_api.h>

namespace gnnScore {

  typedef std::pair<std::vector<float>, std::vector<int64_t>> input_pair;

  struct ONNXOutputNode {
    std::string name;
    ONNXTensorElementDataType type;
    int rank;
  };

  class gnnScoreHandler {

  public:

    // Constructor with parameters
    gnnScoreHandler(std::string model_name);

    // Destructor
    virtual ~gnnScoreHandler();

    // Public methods
    StatusCode initialize();
    int getReqSize();
    //td::vector<float> predict(std::vector<float> inputs, std::vector<std::vector<float>> trackInputs, int ntracks);
    std::vector<float> predict(std::map<std::string, input_pair>& gnn_inputs);

    bool getIsInitialized();

  private:

    // Default constructor
    gnnScoreHandler();

    // Flag to check initialization 
    bool isInitialized = false;

    // Class properties
    std::string m_modelName; // Path to the onnx file
    std::string m_modelPath; // Output of the path resolver

    // Features of the network structure
    size_t m_numInputs;
    size_t m_numOutputs;

    std::vector<int64_t> m_inputDims;
    std::vector<int64_t> m_outputDims;
    std::vector<const char *> m_graphInputNames;
    std::vector<const char *> m_graphOutputNames;

    //// ONNX session objects
    Ort::Env                         m_onnxEnv;
    Ort::SessionOptions              m_onnxSessionOptions;
    Ort::AllocatorWithDefaultOptions m_onnxAllocator;

    std::unique_ptr<Ort::Session> m_onnxSession;
    std::vector<std::string>      m_input_node_names;
    std::vector<std::string>      m_output_node_names;
    std::vector<ONNXOutputNode>   m_output_nodes;

  };

}

#endif
