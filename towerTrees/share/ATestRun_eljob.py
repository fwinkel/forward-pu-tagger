#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

# Este venimos usando
inputFilePath = "/eos/user/f/fwinkel/MET_PU/DAODs/JZ1/" 
ROOT.SH.ScanDir().filePattern( 'DAOD_JETM1.31047751._000181.pool.root.1' ).scan( sh, inputFilePath )
#ROOT.SH.ScanDir().filePattern( 'DAOD_JETM1.31047799._000263.pool.root.1' ).scan( sh, inputFilePath )

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 30000 )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 100 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Adding algorithm sequence

# Set data type to MC
dataType = "mc"

#from nTupleMaker.nTupleMakerAlgorithms import makeSequence
from towerTrees.towerTreesAlgorithms import makeSequence
algSeq = makeSequence (dataType)
#print(algSeq) # For debugging
for alg in algSeq:
    job.algsAdd( alg )
    pass

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'towerTrees', 'AnalysisAlg' )
#alg.ReclusteringSize = 0.6
#alg.MuonPtCut     = 30000.0
#alg.JetPtCut      = 20000.0
#alg.LeadJetsPtCut = 40000.0
#alg.SampleName    = 'H_aa_cccc'

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )

