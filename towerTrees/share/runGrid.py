#!/usr/bin/python
import os, sys
from time import strftime

timestamp = strftime("_%d%m%y")

samples = {
  "mc20_13Tev_jetjet_JZ0WithSW_test"  : "/mc20_13TeV.364700.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0WithSW.deriv.DAOD_JETM1.e7142_a899_r13167_p5331/",   
#  "mc20_13Tev_jetjet_JZ1WithSW"  : "/mc20_13TeV.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.deriv.DAOD_JETM1.e7142_a899_r13167_p5331/",   
#  "mc20_13Tev_jetjet_JZ2WithSW"  : "/mc20_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.deriv.DAOD_JETM1.e7142_a899_r13167_p5331/",   
#  "mc20_13Tev_jetjet_JZ3WithSW"  : "/mc20_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.deriv.DAOD_JETM1.e7142_a899_r13167_p5331/",   
#  "mc20_13Tev_jetjet_JZ4WithSW"  : "/mc20_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.deriv.DAOD_JETM1.e7142_a899_r13167_p5331/",   
}

for outputName, sampleName in samples.items():
    command = 'ATestSubmit_eljob.py --submission-dir=submitDir --outputName='+outputName+timestamp+' --sampleName='+sampleName
    os.system(command)
