#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_option( '-o', '--outputName', dest = 'outputName', action = 'store', type = 'string', default = None, help = 'Name of the generated ntuples' )
parser.add_option( '-n', '--sampleName', dest = 'sampleName', action = 'store', type = 'string', default = None, help = 'Name of the samples (DAODs) to run over' )

( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
ROOT.SH.scanRucio(sh,options.sampleName) 
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 ) #No limits
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 100000 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Adding algorithm sequence

# Set data type to MC
dataType = "mc"

from towerTrees.towerTreesAlgorithms import makeSequence
algSeq = makeSequence (dataType)
#print(algSeq) # For debugging
for alg in algSeq:
    job.algsAdd( alg )
    pass


# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'towerTrees', 'AnalysisAlg' )
#alg.JetPtCut      = 20000.0 

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", "user.fwinkel."+options.outputName)
#driver.submitOnly( job, options.submission_dir )
driver.submitOnly( job, options.outputName )

